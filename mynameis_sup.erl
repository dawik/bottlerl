%% Supervisor and main entry point to use the bot.
%% start_link/0 to fire up the supervisor
%% connect/5 to start a bot instance
%% Valid options are: ssl, autoop, logging, pandora

-module(mynameis_sup).

-behaviour(supervisor).

-export([start_link/0, connect/5, gogo/0]).

-export([init/1]).

start_link() ->
    pandora:start(),
    url:start(),
    ssl:start(),
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

connect(Nickname, Server, Port, Channels, Options) ->
    ChildSpec = {Server, {mynameis, start_link, [Nickname, Server, Port, Channels, Options]},
             permanent, brutal_kill, worker, [mynameis]},
    supervisor:start_child(?MODULE, ChildSpec).

gogo() ->
    ?MODULE:start_link(),
    connect("mynameis", "irc.freequest.net", 6667, ["#styrelserummet"], [{eval, "davve"}, pandora, logging, autoop]),
    connect("mynameis", "irc.efnet.org", 6667, ["#sthd"], [{eval, "davve"}, pandora, logging, autoop]).


init([]) ->
    RestartStrategy = {one_for_one, 60, 1},
    {ok, {RestartStrategy, []}}.
