-module(misc).

-export([string_match/2, string_replace/3, string_extract/3]).

potential_match(String, Match) when hd(String) == hd(Match) ->
    potential_match(tl(String), tl(Match));

potential_match(_, []) ->
    true;

potential_match(_, _) ->
    false.

string_match(String, Match, Pos) when hd(String) == hd(Match) ->
    case potential_match(String, Match) of
        true -> {match, Pos};
        false -> string_match(tl(String), Match, Pos + 1)
    end;

string_match([], _, _) ->
    nomatch;

string_match(String, Match, Pos) ->
    string_match(tl(String), Match, Pos + 1).

string_match(String, Match) when is_binary(String)->
    string_match(binary:bin_to_list(String), Match);

string_match(String, Match) ->
    string_match(String, Match, 0).

string_replace(String, Match, Replacement) ->
    case string_match(String, Match) of
        {match, Pos} when is_list(Replacement) ->
            lists:sublist(String, Pos) ++ Replacement ++ lists:nthtail(Pos + length(Match), String);
        {match, Pos} when is_integer(Replacement) ->
            lists:append([lists:sublist(String, Pos), [Replacement], lists:nthtail(Pos + length(Match), String)]);
        nomatch ->
            String
    end.

string_extract(String, Start, End) ->
    case string_match(String, Start) of
        {match, StartPos} -> 
            Tail = lists:nthtail(StartPos, String), 
            case string_match(Tail, End) of
                {match, EndPos} -> lists:sublist(Tail, EndPos + length(End));
                _ -> nomatch
            end;
        _ -> nomatch
    end.
