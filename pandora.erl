%% Simple interface to talk to pandorabots XML API
%% BotID is set to Administrator bot 
%% start/0 to start deps
%% say/1 to speak to it and return a response

-module(pandora).

-author("dave@douchedata.com").

-export([start/0, say/1]).

start() ->
    inets:start().

say(Something) when is_binary(Something) ->
    say(binary_to_list(Something));

say(Something) ->
    URL = "http://www.pandorabots.com/pandora/talk-xml",
    BotID = "c8c7a9e2be344a53",
    CustID = "a_r_b_i_t_r_a_r_y_s_t_r_i_n_g",
    case httpc:request(post, {URL, [], "application/x-www-form-urlencoded", "botid=" ++ BotID ++ "&input=" ++ Something ++ "&custid=" ++ CustID}, [], []) of
        {ok, {_HTTP,[_,_,_,_,_],Response}} -> 
            case re:run(re:replace(Response,"\n","",[global, {return,list}]), "<that>(.*)</that>", [global, {capture, all, list}]) of
                {_, [[_, Match]] } -> 
                    re:replace(Match, "&quot;","\"",[{return,list}, global]);
                _ -> "Huh"
            end;
        _ -> "Fail"
    end.
